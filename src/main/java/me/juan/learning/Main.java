package me.juan.learning;

import me.juan.learning.particle.Firework;
import me.juan.learning.particle.Particle;
import me.juan.learning.particle.Smoke;

public class Main {
    public static void main(String[] args) {
        Particle firework = ParticleFactory.getOrDefault("firework", (key) -> new Firework());
        Particle smoke = ParticleFactory.getOrDefault("smoke", (key) -> new Smoke());

        firework.explode();
        smoke.explode();
    }
}