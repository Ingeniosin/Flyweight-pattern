package me.juan.learning;

import me.juan.learning.particle.Particle;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class ParticleFactory {

    private static final Map<String, Particle> particleCache = new HashMap<>();

    public static Particle getOrDefault(String type, Function<String, Particle> defaultParticle) {
        return particleCache.computeIfAbsent(type, defaultParticle);
    }
}
