package me.juan.learning.particle;

public class Firework extends Particle {

    public Firework() {
        super("🎆", "Firework");
    }


    @Override
    public void explode() {
        System.out.println("BOOM! " + getDisplay());
    }
}
