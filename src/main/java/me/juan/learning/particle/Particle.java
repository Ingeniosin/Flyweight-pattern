package me.juan.learning.particle;

public abstract class Particle {

    private String display;
    private String type;

    public Particle(String display, String type) {
        this.display = display;
        this.type = type;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public abstract void explode();

}
