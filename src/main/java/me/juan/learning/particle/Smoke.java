package me.juan.learning.particle;

public class Smoke extends Particle {

    public Smoke() {
        super("🌫️", "Smoke");
    }

    @Override
    public void explode() {

    }
}
